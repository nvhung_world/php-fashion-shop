<?php
    $con;
    function Open() {
        global $con;
        $con = mysqli_connect("127.0.0.1:3307","root","hungit","fashionshop");
        if (mysqli_errno($con)) {
            return FALSE;
        } else {
            return TRUE;
        }
    }
    function Close() {
        global $con;
        mysqli_close($con);
    }
    function Query($query) {
        global $con;
        $result = mysqli_query($con,$query);
	if($result)
	{
            return $result;
	}
	else{
            echo mysqli_error($con);
            return NULL;
	}
    }
    function GetDateString()
    {
        $now = getdate();
        $d = $now['mday'];
        $m = $now['mon'];
        $y = $now['year'];
        return "$y-$m-$d";
    }
    function getCurrentPageURL() {
        $pageURL = 'http';

        if (!empty(filter_input(INPUT_SERVER,'HTTPS')))
        {
          if (filter_input(INPUT_SERVER,'HTTPS') == 'on') {
            $pageURL .= "s";
          }
        }
        $pageURL .= "://";
        if (filter_input(INPUT_SERVER,"SERVER_PORT") != "80") {
          $pageURL .= filter_input(INPUT_SERVER,"SERVER_NAME") . ":" . filter_input(INPUT_SERVER,"SERVER_PORT") . filter_input(INPUT_SERVER,"REQUEST_URI");
        } else {
          $pageURL .= filter_input(INPUT_SERVER,"SERVER_NAME") . filter_input(INPUT_SERVER,"REQUEST_URI");
        }

        return $pageURL;
    }
?>
