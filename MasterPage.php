<form name="BodyPage" method="get" style="width: 100%; background-color: #ccc1ad; position: fixed" action = "index.php">
    <table style="width: 90%; margin: 0px auto 0px auto">
        <tr>
            <td rowspan="3" style="width: 150px">
                <div style="padding: 5px 10px 5px 10px">
                    <a href="index.php">
                        <img src="image/ICON.png" alt="" width="150"/>
                    </a>
                </div>
            </td>
            <td colspan="2" style="text-align: right">
                <?php
                    if(isset($_SESSION['MaTK']))
                    {
                        echo $_SESSION['HoTen'];
                        $b=<<<btn
                            <a href="TrangCaNhan.php"
                                <span class="btn btn-warning" >
                                    Trang Cá Nhân
                                </span>
                            </a>  
                            <a href="DangXuat.php"
                                <span class="btn btn-info" >
                                    Đăng Xuất
                                </span>
                            </a>
                            <span>
                                <a href="RoHang.php" style="text-decoration: none">
                                    <img src="image/RoHangIcon.png" alt="" width="30px"/>
                                    Rỏ Hàng
                                </a>
                            </span>                           
btn;
                        echo $b;
                    }
                    else
                    {
                        $b=<<<btn
                            <a href="DangNhap.php"
                                <span class="btn btn-primary" >
                                    Đăng Nhập
                                </span>
                            </a>
                            <a href="DangKy.php"
                                <span class="btn btn-info" >
                                    Đăng Ký
                                </span>
                            </a>                                
btn;
                        echo $b;
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td style="width: 75%">
                <input type="text" name="TimKiem" class="form-control">
            </td>
            <td>
                <input type="submit" name="Submit" value="Tìm Kiếm" class="btn btn-success" width="80%" style="margin-left: 15px">
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div class="dropdown">
                    <div class="dropbtn">
                        <a href="index.php">Trang Chủ</a>
                    </div>
                </div>
                <div class="dropdown">
                    <div class="dropbtn">Thương Hiệu</div>
                    <div class="dropdown-content">
                        <?php
                            $TH = Query("SELECT DISTINCT ThuongHieu FROM thongtinmh");
                            if(empty($TH) == false)
                            {
                                while($row = mysqli_fetch_array($TH))
                                {
                                    echo "<a href=\"index.php?ThuongHieu=$row[0]\">$row[0]</a>";
                                }
                            }
                        ?>
                    </div>
                </div>
                <div class="dropdown">
                    <div class="dropbtn">Đối Tượng</div>
                    <div class="dropdown-content">
                        <?php
                            $DT = Query("SELECT DISTINCT DoiTuong FROM thongtinmh");
                            if(empty($DT) == false)
                            {
                                while($row = mysqli_fetch_array($DT))
                                {
                                    echo "<a href=\"index.php?DoiTuong=$row[0]\">$row[0]</a>";
                                }
                            }
                        ?>
                    </div>
                </div>
                <div class="dropdown">
                    <div class="dropbtn">Loại</div>
                    <div class="dropdown-content">
                        <?php
                            $Loai = Query("SELECT DISTINCT Loai FROM mathang;");
                            if(empty($Loai) == false)
                            {
                                while($row = mysqli_fetch_array($Loai))
                                {
                                    echo "<a href=\"index.php?Loai=$row[0]\">$row[0]</a>";
                                }
                            }
                        ?>
                    </div>
                </div>
                <div class="dropdown">
                    <div class="dropbtn">Năm</div>
                    <div class="dropdown-content">
                        <?php
                            $Nam = Query("SELECT DISTINCT Nam FROM thongtinmh");
                            if(empty($Nam) == false)
                            {
                                while($row = mysqli_fetch_array($Nam))
                                {
                                    echo "<a href=\"index.php?Nam=$row[0]\">$row[0]</a>";
                                }
                            }
                        ?>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</form>
<?php
    unset($Nam);
    unset($Loai);
    unset($DT);
    unset($TH);
    unset($b);
    unset($row);
?>