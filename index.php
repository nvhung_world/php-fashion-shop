<?php
    include './Unit.php';
    session_start();
    $_SESSION['OldUrl'] = getCurrentPageURL();
    Open();
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Fashion Shop</title>
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/DropdownList.css" rel="stylesheet" type="text/css"/>
        <link href="css/MH.css" rel="stylesheet" type="text/css"/>
    </head>
    <body style="margin: 0px 0px 0px 0px">
        <div>
            <?php
                include './MasterPage.php';
            ?>
        </div>
        <div style="width: 100%; padding-top: 200px">
            <form name="HeadPage" method="get" style="width: 90%; margin: auto auto auto auto">
                <div>
                    <table style="width: 100%; margin: auto auto auto auto">
                        <?php
                            $Other = "";
                            if(filter_input(INPUT_GET, 'TimKiem'))
                            {
                                $value = filter_input(INPUT_GET, 'TimKiem');
                                $Other = " and upper(TenMH) like upper('%$value%')";
                            }
                            if(filter_input(INPUT_GET, 'ThuongHieu'))
                            {
                                $value = filter_input(INPUT_GET, 'ThuongHieu');
                                $Other = " and ThuongHieu = '$value'";
                            }
                            if(filter_input(INPUT_GET, 'DoiTuong'))
                            {
                                $value = filter_input(INPUT_GET, 'DoiTuong');
                                $Other = " and DoiTuong = '$value'";
                            }
                            if(filter_input(INPUT_GET, 'Loai'))
                            {
                                $value = filter_input(INPUT_GET, 'Loai');
                                $Other = " and Loai = '$value'";
                            }
                            if(filter_input(INPUT_GET, 'Nam'))
                            {
                                $value = filter_input(INPUT_GET, 'Nam');
                                $Other = " and Nam = '$value'";
                            }
                            $StartSelect = "0";
                            if(filter_input(INPUT_GET, 'Trang'))
                            {
                                $StartSelect = filter_input(INPUT_GET, 'Trang') * 20;
                            }
                            $Query = "SELECT mathang.MaMH, TenMH, GiaBan, Avatar, ThuongHieu, DanhGia, LuotXem, MoTaNgan  FROM thongtinmh, mathang where thongtinmh.MaMH = mathang.MaMH $Other limit $StartSelect,20";
                            $result = Query($Query);
                            if(empty($result) == false)
                            {
                                $rowCout = mysqli_affected_rows($con);
                                for($i = 0; $i < (round($rowCout/5) < $rowCout/5 ? round($rowCout/5) + 1: round($rowCout/5)); $i++)
                                {
                                    echo "<tr>";
                                    for($j = 0; $j <  5; $j++)
                                    {
                                        $row = mysqli_fetch_row($result);
                                        if(empty($row) == FALSE)
                                        {
                                $a = <<<doc
<div class="IconMH">
    <a href="ChiTietMatHang.php?MaMH=$row[0]">
        <table style="width: 90%; margin-left: auto; margin-right: auto">
            <tr>
                <td colspan="2">
                    <div style="width:100%;height: 250px; background-color: #cccccc; margin: 5px auto 5px auto">
                        <img src="$row[3]" alt="" width="100%" height = "100%"/>
                    </div>
doc;
        
                                $b = <<<doc
                </td>
            </tr>
            <tr>
                <td style="text-align: left">$row[1]</td>
                <td style="text-align: right">$row[2] đ</td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center">$row[7]</td>
            </tr>
            <tr>
                <td style="text-align: left">Vote: $row[5]</td>
                <td style="text-align: right">View: $row[6]</td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center">$row[4]</td>
            </tr>
        </table>
    </a>
</div>
doc;
                                
                                            echo "<td width=\"20%\">$a";
                                            echo "$b</td>";
                                        }
                                        else {
                                            echo "<td> </td>";
                                        }
                                    }
                                    echo "</tr>";
                                }
                            }
                        ?>
                    </table>
                </div>
            </form>
        </div>
        <div style="width: 100%; background: #ccc1ad; bottom: 0; margin-top: 20px">
            <?php
                include "./BottomPage.php";
            ?>
        </div>
    </body>
</html>
<?php
    unset($b);
    unset($a);
    unset($row);
    unset($rowCout);
    unset($result);
    unset($Query);
    Close();
?>