<?php
    include './Unit.php';
    session_start();
    $_SESSION['OldUrl'] = getCurrentPageURL();
    Open();
    $MaDH;
    $NgayBan;
    $TrangThai;
    $NoiGiao;
    $TongTien;
    $TongSanPham;
    if(isset($_SESSION['MaTK']) == TRUE)
    {
        $MaTK = $_SESSION['MaTK'];
        $res = Query("SELECT donhang.MaDH, NgayBan, TrangThai, NoiGiao FROM donhang, taikhoan where donhang.MaTK = taikhoan.MaTK and donhang.TrangThai != 'Đã Thanh Toán' and taikhoan.MaTK = $MaTK");
        if(empty($res) == FALSE)
        {
            $row = mysqli_fetch_array($res);
            if(empty($row)==false)
            {
                $_SESSION['MaDH'] = $row[0];
                $MaDH = $row[0];
                $NgayBan = $row[1];
                $TrangThai = $row[2];
                $NoiGiao = $row[3];
                if(filter_input(INPUT_GET, 'TrangThaiDH') && filter_input(INPUT_GET, 'NoiGiao'))
                {
                    $TrangThai = filter_input(INPUT_GET, 'TrangThaiDH');
                    if($TrangThai == "Xác nhận mua"){
                        $TrangThai = "Đang Chuyển";
                    }
                    else{
                        $TrangThai = "Giỏ Hàng";
                    }
                    $NoiGiao = filter_input(INPUT_GET, 'NoiGiao');
                    Query("UPDATE donhang SET TrangThai = '$TrangThai', NoiGiao = '$NoiGiao' WHERE MaDH = $MaDH;");
                }
                if(filter_input(INPUT_GET, 'HuyDH'))
                {
                    $IDDH = filter_input(INPUT_GET, 'HuyDH');
                    Query("DELETE FROM chitietdh WHERE IDCTDH = $IDDH");
                }
            }
            else
            {
                $NgayBan = GetDateString();
                Query("INSERT INTO donhang(MaTK,NgayBan,TrangThai)VALUES($MaTK, '$NgayBan', 'Giỏ Hàng');");
                $_SESSION['MaDH'] = $con->insert_id;
                $MaDH = $con->insert_id;
                $TrangThai = 'Giỏ Hàng';
                $NoiGiao = "";
            }
        }
        $res_2 = Query("select sum(GiaBan), sum(SoLuong)  from donhang, chitietdh, mathang, thongtinmh where donhang.MaDH = chitietdh.MaDH and chitietdh.MaMH = mathang.MaMH and mathang.MaMH = thongtinmh.MaMH and donhang.MaDH = $MaDH");
        if(empty($res_2) == FALSE)
        {
            $row = mysqli_fetch_array($res_2);
            $TongTien = $row[0];
            $TongSanPham = $row[1];
        }
    }
    else
    {
        header ("Location: DangNhap.php");
    }
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Fashion Shop</title>
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/DropdownList.css" rel="stylesheet" type="text/css"/>
    </head>
    <body style="margin: 0px 0px 0px 0px">
        <div>
            <?php
                include './MasterPage.php';
            ?>
        </div>
        <div style="width: 100%; padding-top: 200px">
            <form method="get" style="width: 90%; margin: auto auto auto auto">
                <table style="width: 100%">
                    <tr>
                        <td class="panel panel-default" style="width: 300px">
                            <table class="table-condensed table-hover" style="width: 95%; margin: auto auto auto auto;">
                                <tr>
                                    <td>
                                        Khách hàng
                                    </td>
                                    <td>
                                        <?php
                                            echo $_SESSION['HoTen'];
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Tổng sản phẩm
                                    </td>
                                    <td>
                                        <?php
                                            echo "$TongSanPham";
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Tổng Tiền
                                    </td>
                                    <td>
                                        <?php
                                            echo "$TongTien";
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Ngày Bán
                                    </td>
                                    <td>
                                        <?php
                                            echo "$NgayBan";
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        Nơi Giao
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <textarea name="NoiGiao" maxlength="100" style="width: 100%; max-width: 300px;max-height: 150px">
                                            <?php
                                                echo "$NoiGiao";
                                            ?>
                                        </textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <?php
                                            $b; 
                                            if($TrangThai == 'Giỏ Hàng'){
                                                $b = <<<doc
                                                    <input type="submit" name="TrangThaiDH" value="Xác nhận mua" class="btn btn-success" style="width: 100%">
doc;
                                            }
                                            else
                                            {
                                                $b = <<<doc
                                                    <input type="submit" name="TrangThaiDH" value="Hủy Đơn Hàng" class="btn btn-success" style="width: 100%">
doc;
                                            }
                                            echo $b;
                                        ?>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <table class="table-condensed" style="width: 90%; margin: auto auto auto auto">
                                <?php
                                    $res = Query("select TenMH, GiaBan, ThuongHieu, Avatar, Nam, DanhGia, LuotXem, MoTaNgan, IDCTDH   from donhang, chitietdh, mathang, thongtinmh where donhang.MaDH = chitietdh.MaDH and chitietdh.MaMH = mathang.MaMH and mathang.MaMH = thongtinmh.MaMH and donhang.MaDH = $MaDH");
                                    if(empty($res) == FALSE)
                                    {
                                        while ($row = mysqli_fetch_array($res))
                                        {
                                            $c= <<<doc
<table class="table-condensed" style="width: 95%; margin: auto auto auto auto">
    <tr>
        <td rowspan="8" style="width: 250px">
            <div class="panel panel-default" style="width: 250px; height: 250px">
                <img src="$row[3]" alt="" width="100%" height = "100%"/>
            <div>
        </td>
        <td style="width: 50px">
            Tên Mặt Hàng
        </td>
        <td>
            $row[0]                                               
        </td>
    </tr>
    <tr>
        <td>
            Giá Bán
        </td>
        <td>
            $row[1]                                            
        </td>
    </tr>
    <tr>
        <td>
            Thương Hiệu
        </td>
        <td>
            $row[2]                                            
        </td>
    </tr>
    <tr>
        <td>
            Năm Xản Xuất
        </td>
        <td>
            $row[4]                                        
        </td>
    </tr>
    <tr>
        <td>
            Đánh Giá
        </td>
        <td>
            $row[5]
        </td>
    </tr>
    <tr>
        <td>
            Lượt Xem
        </td>
        <td>
            $row[6]
        </td>
    </tr>
    <tr>
        <td>
            Mô Tả
        </td>
        <td>
            $row[7]
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <a href="RoHang.php?HuyDH=$row[8]" class="btn btn-danger" style="width: 80%; margin: auto auto auto auto">Hủy</a>
        </td>
    </tr>                                                
</table>
doc;
                                            echo $c;
                                        }
                                    }
                                ?>
                            </table>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
        <div style="width: 100%; background: #ccc1ad; bottom: 0; margin-top: 20px">
            <?php
                include "./BottomPage.php";
            ?>
        </div>
    </body>
</html>
<?php
    unset($res);
    unset($row);
    unset($c);
    unset($b);
    unset($res_3);
    unset($res_2);
    unset($MaTK);
    unset($TongSanPham);
    unset($TongTien);
    unset($NoiGiao);
    unset($TrangThai);
    unset($NgayBan);
    unset($MaDH);
    Close();
?>