<?PHP
    include './Unit.php';
    session_start();
    $error = "";
    Open();
    if(filter_input(INPUT_POST, 'user') &&
        filter_input(INPUT_POST, 'pass')&& 
        filter_input(INPUT_POST, 'pass2')&&
        filter_input(INPUT_POST, 'Ho')&& 
        filter_input(INPUT_POST, 'Ten')&& 
        filter_input(INPUT_POST, 'GioiTinh'))
    {
        $user = filter_input(INPUT_POST, 'user');
        $pass = filter_input(INPUT_POST, 'pass');
        $pass2 = filter_input(INPUT_POST, 'pass2');
        $Ho = filter_input(INPUT_POST, 'Ho');
        $Ten = filter_input(INPUT_POST, 'Ten');
        $GioiTinh = filter_input(INPUT_POST, 'GioiTinh');
        $KetQua = KiemTra($user, $pass, $pass2);
        if($KetQua == "")
        {
            $now = GetDateString();
            $res_1 = Query("INSERT INTO taikhoan(TaiKhoan,MatKhau,Quyen,NgayLap)VALUES('$user', '$pass', 'Khách Hàng', '$now')");
            if( empty($res_1) == FALSE)
            {
                $Last_ID = $con->insert_id;
                $res_2 = Query("INSERT INTO thongtintk(matk, ho, ten, gioitinh)VALUES($Last_ID, '$Ho', '$Ten', '$GioiTinh')");
                if(empty($res_2) == FALSE)
                {
                    $_SESSION['MaTK'] = $Last_ID;
                    $_SESSION['Quyen'] = "Khách Hàng";
                    $_SESSION['user'] = $user;
                    $_SESSION['HoTen'] = $Ho.' '.$Ten;
                }
                else{
                    $error = $res_2;
                }
            }
            else{
                $error = $res_1;
            }
        }else
        {
            $error = $KetQua;
        }
    }
    if(isset($_SESSION['MaTK']))
    {
        $oldURL = $_SESSION['OldUrl'];
        header ("Location: $oldURL");
    }
    function KiemTra($user, $pass, $pass2)
    {
        if (strlen($user) < 4) {
            return "Tài khoản phải dài hơn 4 kí tự";
        }
        if (strlen($pass) < 4) {
            return "Mật khẩu phải dài hơn 4 kí tự";
        }
        if ($pass != $pass2) {
            return "Tài khoản phải dài hơn 4 kí tự";
        }
        $result = Query("select MaTK from taikhoan where upper(TaiKhoan) = upper('$user') and MatKhau = '$pass'");
        if(empty($result) == FALSE )
        {
            $row = mysqli_fetch_array($result);
            if(empty($row)== false)
            {
                return "Tài khoản đã tồn tại";
            }
        }
        return "";
    }
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Fashion Shop</title>
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
    </head>
    <body style="background-color: #cccccc">
        <?php
            // put your code here
        ?>
        <form method="post">
            <div class="panel panel-default" style="width: 500px; margin: 20px auto auto auto">
                <div class="panel-heading" style="text-align: center; font-weight: bolder; font-size: 30px; color: #ff9900">
                    ĐĂNG KÝ
                </div>
                <div class="panel-body">
                    <table style="width: 90%; margin: 10px auto 10px auto" class="table-condensed table-hover">
                        <tr>
                            <td>
                                Họ
                            </td>
                            <td>
                                <input type="text" name="Ho" class="form-control" style="width: 100%" maxlength="15">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Tên
                            </td>
                            <td>
                                <input type="text" name="Ten" class="form-control" style="width: 100%" maxlength="20">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Giới Tính
                            </td>
                            <td>
                                <input type="radio" name="GioiTinh" value="Nam"> Nam
                                <input type="radio" name="GioiTinh" value="Nữ"> Nữ
                                <input type="radio" name="GioiTinh" value="Không Dõ" checked="true"> Không Dõ
                            </td>
                        </tr>
                    </table>
                    <table style="width: 90%; margin: 10px auto 10px auto" class="table-condensed table-hover">
                        <tr>
                            <td>
                                Tài Khoản
                            </td>
                            <td>
                                <input type="text" name="user" class="form-control" style="width: 100%" maxlength="30">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Mật Khẩu
                            </td>
                            <td>
                                <input type="password" name="pass" class="form-control"  style="width: 100%" maxlength="30">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Nhập Lại Mật Khẩu
                            </td>
                            <td>
                                <input type="password" name="pass2" class="form-control"  style="width: 100%" maxlength="30">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <input type="submit" name="DangKy" value="Đăng Ký" class="btn btn-primary" style="width: 100%">
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="panel-footer" style="color: red">
                    <?php
                        echo $error;
                    ?>
                </div>
            </div>
        </form>
    </body>
</html>
<?php
    unset($error);
    unset($row);
    unset($result);
    unset($pass);
    unset($pass2);
    unset($user);
    unset($oldURL);
    unset($KetQua);
    unset($Ho);
    unset($Ten);
    unset($res_2);
    unset($res_1);
    unset($now);
    unset($GioiTinh);
    Close();
?>
