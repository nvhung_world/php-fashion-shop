<?php
    include './Unit.php';
    session_start();
    $_SESSION['OldUrl'] = getCurrentPageURL();
    Open();
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Fashion Shop</title>
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/DropdownList.css" rel="stylesheet" type="text/css"/>
    </head>
    <body style="margin: 0px 0px 0px 0px">
        <div>
            <?php
                include './MasterPage.php';
            ?>
        </div>
        <div style="width: 100%; padding-top: 200px">
            
        </div>
        <div style="width: 100%; background: #ccc1ad; bottom: 0; margin-top: 20px">
            <?php
                include "./BottomPage.php";
            ?>
        </div>
    </body>
</html>
<?php
    Close();
?>