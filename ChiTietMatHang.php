<?php
    include './Unit.php';
    session_start();
    $_SESSION['OldUrl'] = getCurrentPageURL();
    Open();
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Fashion Shop</title>
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/DropdownList.css" rel="stylesheet" type="text/css"/>
    </head>
    <body style="margin: 0px 0px 0px 0px">
        <div>
            <?php
                include './MasterPage.php';
            ?>
        </div>
        <form method="get" style="width: 100%; padding-top: 200px">
            <?php
                $MaMH;
                $row;
                $LuotXem = 0;
                $LuongMua = 0;
                if(filter_input(INPUT_GET, 'MaMH'))
                {
                    $MaMH = filter_input(INPUT_GET, 'MaMH');
                    $LuotXem = 1;
                    $_SESSION['SelectMH'] = $MaMH;
                }else if(isset($_SESSION['SelectMH']) == TRUE)
                {
                    $MaMH = $_SESSION['SelectMH'];
                    $LuotXem = 0;
                }else
                {
                    header ("Location: index.php");
                }
                if ( empty($MaMH) == FALSE) 
                {
                    $result = Query("Select Avatar, ThuongHieu, DoiTuong, Nam, DanhGia, LuotXem, MoTaNgan, TenMH, Loai, GiaBan from thongtinmh, mathang where mathang.MaMH = thongtinmh.MaMH and mathang.MaMH = $MaMH");
                    if(empty($result) == FALSE)
                    {
                        $row = mysqli_fetch_array($result);
                        if(empty($row)==false)
                        {
                            $LuotXem = $LuotXem + (int)$row[5];
                            Query("UPDATE thongtinmh SET LuotXem = $LuotXem WHERE MaMH = $MaMH");
                        }
                    }
                    if(filter_input(INPUT_GET,'Mua'))
                    {
                        if(isset($_SESSION['MaTK']))
                        {
                            $matk = $_SESSION['MaTK'];
                            if(isset($_SESSION['MaDH']) == false)
                            {
                                $result = Query("SELECT MaDH FROM donhang, taikhoan where donhang.MaTK = taikhoan.MaTK and donhang.TrangThai = 'Giỏ Hàng' and taikhoan.MaTK = $matk");
                                if(empty($result) == FALSE)
                                {
                                    $row2 = mysqli_fetch_array($result);
                                    if(empty($row2)==false)
                                    {
                                        $_SESSION['MaDH'] = $row2[0];
                                    }
                                    else
                                    {
                                        $NgayBan = GetDateString();
                                        Query("INSERT INTO donhang(MaTK,NgayBan,TrangThai)VALUES($matk, '$NgayBan', 'Giỏ Hàng');");
                                        $_SESSION['MaDH'] = $con->insert_id;
                                    }
                                }
                            }
                            if(isset($_SESSION['MaCTDH']) == false)
                            {
                                $MaDH = $_SESSION['MaDH'];
                                $result = Query("select IDCTDH from chitietdh, donhang, mathang where donhang.MaDH = chitietdh.MaDH and chitietdh.MaMH = mathang.MaMH and donhang.MaDH = $MaDH and mathang.MaMH = $MaMH");
                                if(empty($result) == FALSE)
                                {
                                    $row2 = mysqli_fetch_array($result);
                                    if(empty($row2)==false)
                                    {
                                        $_SESSION['IDCTMH'] = $row2[0];
                                    }
                                    else
                                    {
                                        $NgayBan = GetDateString();
                                        Query("INSERT INTO chitietdh(MaDH,MaMH)VALUES($MaDH, $MaMH);");
                                        $_SESSION['IDCTMH'] = $con->insert_id;
                                    }
                                }
                            }
                            $IDCTDH = $_SESSION['IDCTMH'];
                            $result = Query("SELECT SoLuong FROM chitietdh where IDCTDH = $IDCTDH;");
                            if(empty($result) == FALSE)
                            {
                                $row2 = mysqli_fetch_array($result);
                                if(empty($row2)==false)
                                {
                                    $LuongMua = $row2[0] + 1;
                                    Query("UPDATE chitietdh SET SoLuong = $LuongMua WHERE IDCTDH = $IDCTDH;");
                                }
                            }
                        }
                        else
                        {
                            header ("Location: DangNhap.php");
                        }
                    }
                }
            ?>
            <div class="panel panel-default" style="width: 50%; margin: auto auto auto auto">
                <table class="table table-condensed">
                    <tr>
                        <td rowspan="8" style="width: 250px">
                            <span class="img-thumbnail" style="width: 250px; height: 250px; margin: 7px 7px 7px 7px">
                                <img src="<?php echo $row[0] ?>" width="100%" height="100%"/>
                            </span>
                        </td>
                        <td style="width: 200px">
                            Tên Mặt Hàng
                        </td>
                        <td>
                            <?php
                                echo $row[7];
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Giá Bán
                        </td>
                        <td>
                            <?php
                                echo $row[9]." đ";
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Vote
                        </td>
                        <td>
                            <?php
                                echo $row[4];
                            ?>
                            <input type="submit" name="Vote" value=" + 1 " class="btn btn-danger">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            View
                        </td>
                        <td>
                            <?php
                                echo $row[5];
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Loại
                        </td>
                        <td>
                            <?php
                                echo $row[8];
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Đối Tượng
                        </td>
                        <td>
                            <?php
                                echo $row[2];
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Sản Phẩm Năm
                        </td>
                        <td>
                            <?php
                                echo $row[3];
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Thương Hiệu
                        </td>
                        <td>
                            <?php
                                echo $row[1];
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            Mô Tả
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <?php
                                echo $row[6];
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td>
                            <input type="submit" name="Mua" value="Cho Vào Giỏ" class="btn btn-success" style="width: 200px">
                        </td>
                        <td>
                            <?php
                                echo "+ $LuongMua trong giỏ hàng";
                            ?> 
                        </td>
                    </tr>
                </table>
            </div>
        </form>
        <div style="width: 100%; background: #ccc1ad; bottom: 0; margin-top: 20px">
            <?php
                include "./BottomPage.php";
            ?>
        </div>
    </body>
</html>
<?php
    unset($LuongMua);
    unset($row);
    unset($row2);
    unset($result);
    unset($IDCTDH);
    unset($NgayBan);
    unset($MaDH);
    unset($matk);
    unset($LuotXem);
    unset($MaMH);
    Close();
?>
