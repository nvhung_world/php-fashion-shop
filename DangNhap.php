<?PHP
    include './Unit.php';
    session_start();
    $error = "";
    Open();
    if(filter_input(INPUT_POST, 'user') && filter_input(INPUT_POST, 'pass'))
    {
        $user = filter_input(INPUT_POST, 'user');
        $pass = filter_input(INPUT_POST, 'pass');
        $result = Query("select taikhoan.MaTK, Quyen, concat(ho, ' ', ten) as 'HoTen' from taikhoan, thongtintk where taikhoan.MaTK = thongtintk.MaTK and upper(TaiKhoan) = upper('$user') and MatKhau = '$pass'");
        if(empty($result) == FALSE )
        {
            $row = mysqli_fetch_array($result);
            if(empty($row)== false)
            {
                $_SESSION['MaTK'] = $row[0];
                $_SESSION['Quyen'] = $row[1];
                $_SESSION['user'] = $user;
                $_SESSION['HoTen'] = $row[2];
            }
            else
            {
                $error = "Tài khoản không tồn tại";
            }
        }
        else
        {
            $error = "Lỗi CSDL";
        }
    }
    if(isset($_SESSION['MaTK']))
    {
        $oldURL = $_SESSION['OldUrl'];
        header ("Location: $oldURL");
    }
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Fashion Shop</title>
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
    </head>
    <body style="background-color: #cccccc">
        <?php
            // put your code here
        ?>
        <form method="post">
            <div class="panel panel-default" style="width: 400px; margin: 20px auto auto auto">
                <div class="panel-heading" style="text-align: center; font-weight: bolder; font-size: 30px; color: #ff9900">
                    ĐĂNG NHẬP
                </div>
                <div class="panel-body">
                    <table style="width: 90%; margin: 10px auto 10px auto" class="table-condensed table-hover">
                        <tr>
                            <td>
                                Tài Khoản
                            </td>
                            <td>
                                <input type="text" name="user" class="form-control" style="width: 100%">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Mật Khẩu
                            </td>
                            <td>
                                <input type="password" name="pass" class="form-control"  style="width: 100%">
                            </td>
                        </tr>
                    </table>
                    <table style="width: 90%; margin: 15px auto 10px auto" class="table-condensed">
                        <tr>
                            <td colspan="2">
                                <input type="checkbox" name="LuuPass"> Lưu mật khẩu
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <input type="submit" name="DangNhap" value="Đăng Nhập" class="btn btn-success" style="width: 100%">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href="">Quên mật khẩu</a>
                            </td>
                            <td  style="text-align: right">
                                <a href="DangKy.php"
                                    <div class="btn btn-primary"  style="width: 150px">
                                        Đăng Ký
                                    </div>
                                </a>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="panel-footer" style="color: ref">
                    <?php
                        echo $error;
                    ?>
                </div>
            </div>
        </form>
    </body>
</html>
<?php
    unset($oldURL);
    unset($error);
    unset($row);
    unset($result);
    unset($pass);
    unset($user);
    Close();
?>
